﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Capa_Servicio
{
    /// <summary>
    /// Descripción breve de Insertar
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class Insertar : System.Web.Services.WebService
    {

        [WebMethod]
        public string addingrediente(string Nombre, string Descrip, string Vida)
        {
            DllPrincipal.Datos d = new DllPrincipal.Datos();
            string respuesta = "";
            respuesta = d.addingrediente(Nombre, Descrip, Vida);
            return respuesta;

        }
    }
}
