﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DllPrincipal
{
    public class Datos
    {
        private dbDatosDataContext dcontext = new dbDatosDataContext();


        public string addingrediente(string Nombre, string Descrip, string Vida)
        {
            try
            {
                ingrediente ingre = new ingrediente();
                ingre.nombre_ingrediente = Nombre;
                ingre.descrip_ingrediente = Descrip;
                ingre.vidautil_ingrediente = Vida;

                dcontext.ingrediente.InsertOnSubmit(ingre);
                dcontext.SubmitChanges();

                return "Datos Ingresados Correctamente";
            }//fin del try
            catch(Exception ex)
            {
                return "Error : " + ex.Message.ToString();
            }//fin del catch

        }//fin del string addingrediente

    }//fin de la clase datos

}//fin del namespace
