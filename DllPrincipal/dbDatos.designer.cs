﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DllPrincipal
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="jhons1")]
	public partial class dbDatosDataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Definiciones de métodos de extensibilidad
    partial void OnCreated();
    partial void Insertingrediente(ingrediente instance);
    partial void Updateingrediente(ingrediente instance);
    partial void Deleteingrediente(ingrediente instance);
    #endregion
		
		public dbDatosDataContext() : 
				base(global::DllPrincipal.Properties.Settings.Default.jhons1ConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public dbDatosDataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public dbDatosDataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public dbDatosDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public dbDatosDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public System.Data.Linq.Table<ingrediente> ingrediente
		{
			get
			{
				return this.GetTable<ingrediente>();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.ingrediente")]
	public partial class ingrediente : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _id_ingrediente;
		
		private string _nombre_ingrediente;
		
		private string _descrip_ingrediente;
		
		private string _vidautil_ingrediente;
		
    #region Definiciones de métodos de extensibilidad
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void Onid_ingredienteChanging(int value);
    partial void Onid_ingredienteChanged();
    partial void Onnombre_ingredienteChanging(string value);
    partial void Onnombre_ingredienteChanged();
    partial void Ondescrip_ingredienteChanging(string value);
    partial void Ondescrip_ingredienteChanged();
    partial void Onvidautil_ingredienteChanging(string value);
    partial void Onvidautil_ingredienteChanged();
    #endregion
		
		public ingrediente()
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_id_ingrediente", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public int id_ingrediente
		{
			get
			{
				return this._id_ingrediente;
			}
			set
			{
				if ((this._id_ingrediente != value))
				{
					this.Onid_ingredienteChanging(value);
					this.SendPropertyChanging();
					this._id_ingrediente = value;
					this.SendPropertyChanged("id_ingrediente");
					this.Onid_ingredienteChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_nombre_ingrediente", DbType="VarChar(30) NOT NULL", CanBeNull=false)]
		public string nombre_ingrediente
		{
			get
			{
				return this._nombre_ingrediente;
			}
			set
			{
				if ((this._nombre_ingrediente != value))
				{
					this.Onnombre_ingredienteChanging(value);
					this.SendPropertyChanging();
					this._nombre_ingrediente = value;
					this.SendPropertyChanged("nombre_ingrediente");
					this.Onnombre_ingredienteChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_descrip_ingrediente", DbType="VarChar(30) NOT NULL", CanBeNull=false)]
		public string descrip_ingrediente
		{
			get
			{
				return this._descrip_ingrediente;
			}
			set
			{
				if ((this._descrip_ingrediente != value))
				{
					this.Ondescrip_ingredienteChanging(value);
					this.SendPropertyChanging();
					this._descrip_ingrediente = value;
					this.SendPropertyChanged("descrip_ingrediente");
					this.Ondescrip_ingredienteChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_vidautil_ingrediente", DbType="VarChar(30) NOT NULL", CanBeNull=false)]
		public string vidautil_ingrediente
		{
			get
			{
				return this._vidautil_ingrediente;
			}
			set
			{
				if ((this._vidautil_ingrediente != value))
				{
					this.Onvidautil_ingredienteChanging(value);
					this.SendPropertyChanging();
					this._vidautil_ingrediente = value;
					this.SendPropertyChanged("vidautil_ingrediente");
					this.Onvidautil_ingredienteChanged();
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
#pragma warning restore 1591
